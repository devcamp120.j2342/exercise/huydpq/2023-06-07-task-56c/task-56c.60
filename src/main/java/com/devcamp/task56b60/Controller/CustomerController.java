package com.devcamp.task56b60.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task56b60.Service.CustomerService;
import com.devcamp.task56b60.models.Customer;

@RestController
@CrossOrigin
public class CustomerController {
    @Autowired
    private CustomerService customerService;
    public ArrayList <Customer> getAllCustomer(){
        ArrayList <Customer> customers = customerService.getAllCustomer();

        return customers;
    }
}
