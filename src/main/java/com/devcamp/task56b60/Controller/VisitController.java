package com.devcamp.task56b60.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task56b60.Service.VisitService;
import com.devcamp.task56b60.models.Visit;

@RestController
@CrossOrigin
public class VisitController {
    @Autowired
    private VisitService visitService;
    @GetMapping("/visit")

    public ArrayList<Visit> getVisits(){
         ArrayList<Visit> arrListVisit = visitService.getAllVisit();

        return arrListVisit;
    }
}
