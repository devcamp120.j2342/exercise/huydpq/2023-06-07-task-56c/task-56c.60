package com.devcamp.task56b60.models;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Visit {
   private Customer customer;
    private Date date;
    private double serviceExpense;
    private double productExpense;


    public Visit(Customer customer, Date date) {
        this.customer = customer;
        this.date = date;
    }
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getServiceExpense() {
        return serviceExpense;
    }

    public void setServiceExpense(double serviceExpense) {
        this.serviceExpense = serviceExpense;
    }

    public double getProductExpense() {
        return productExpense;
    }

    public void setProductExpense(double productExpense) {
        this.productExpense = productExpense;
    }

    public double getTotalExpence(){
        return serviceExpense + productExpense;
    }

    @Override
    public String toString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        String dateFormatDate = dateFormat.format(date);
        return "Visit [customer=" + customer + ", date=" + dateFormatDate + ", serviceExpense=" + serviceExpense
                + ", productExpense=" + productExpense + "]";
    }

    
    

}
