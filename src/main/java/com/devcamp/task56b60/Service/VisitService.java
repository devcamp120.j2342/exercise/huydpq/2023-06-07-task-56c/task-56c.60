package com.devcamp.task56b60.Service;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task56b60.models.Visit;

@Service
public class VisitService {
    @Autowired
    private CustomerService customerService;
public ArrayList<Visit> getAllVisit(){
    Visit visit1 = new Visit(customerService.getAllCustomer().get(0), new Date());
    Visit visit2 = new Visit(customerService.getAllCustomer().get(1), new Date());
    Visit visit3 = new Visit(customerService.getAllCustomer().get(2), new Date());

    ArrayList<Visit> visits = new ArrayList<>();
    visits.add(visit1);
    visits.add(visit2);
    visits.add(visit3);

    return visits;
}
    
}
