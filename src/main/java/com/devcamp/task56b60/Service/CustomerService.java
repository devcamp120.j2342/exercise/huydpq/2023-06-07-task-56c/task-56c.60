package com.devcamp.task56b60.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.task56b60.models.Customer;

@Service
public class CustomerService {
    Customer customer1 = new Customer("Huy");
    Customer customer2 = new Customer("Lan ");
    Customer customer3 = new Customer("Trâm");

    public ArrayList<Customer> getAllCustomer() {
        customer1.setMember(false);
        customer2.setMember(true);
        customer3.setMember(true);

        customer1.setMemberType("aa");
        customer2.setMemberType("bbb");
        customer3.setMemberType("ccc");

        ArrayList<Customer> customers = new ArrayList<>();
        customers.add(customer1);
        customers.add(customer2);
        customers.add(customer3);

        return customers;
    }
}
